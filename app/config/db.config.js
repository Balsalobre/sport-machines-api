const env = require('./env.js');
 
const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  dialect: env.dialect,
  operatorsAliases: false,
 
  pool: {
    max: env.max,
    min: env.pool.min,
    acquire: env.pool.acquire,
    idle: env.pool.idle
  }
});
 
const db = {};
 
db.Sequelize = Sequelize;
db.sequelize = sequelize;
 
// Creación de las tablas en la base de datos
db.user = require('../model/user.model')(sequelize, Sequelize);
db.role = require('../model/role.model')(sequelize, Sequelize);
db.machine = require('../model/machine.model')(sequelize, Sequelize);
db.userOauth = require('../model/userOauth.model')(sequelize, Sequelize);
db.characteristics = require('../model/characteristics.model')(sequelize, Sequelize);

// Creación de las relaciones entre tablas
db.role.belongsToMany(db.user, { through: 'user_roles', foreignKey: 'roleId', otherKey: 'userId'});
db.user.belongsToMany(db.role, { through: 'user_roles', foreignKey: 'userId', otherKey: 'roleId'});
db.characteristics.hasOne(db.user, {as: 'id_userChar', foreignKey : 'id_char'});
db.characteristics.hasOne(db.userOauth, {as: 'id_userOauthChar', foreignKey : 'id_char'});
module.exports = db;