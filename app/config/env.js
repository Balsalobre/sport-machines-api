const keys = require('./keys');

const env = {
  database: 'sportmachines',
  username: 'root',
  password: keys.dbPassword,
  host: 'localhost',
  dialect: 'mysql',
  pool: {
	  max: 5,
	  min: 0,
	  acquire: 30000,
	  idle: 10000
  }
};
 
module.exports = env;