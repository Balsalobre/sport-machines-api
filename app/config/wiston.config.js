const winston = require('winston');

// process.on('uncaughtException', (ex) => {
// 	console.log('WE GOT AN UNCAUGHT EXCEPTION');
// 	winston.error(ex.message, ex);
// });

// process.on('unhandledRejection', (ex) => {
// 	console.log('WE GOT AN UNHANDLED REJECTION');
// 	winston.error(ex.message, ex);
// });

var options = {
    file: {
        level: 'info',
        filename: `${__dirname}/../../logs/log-api.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: true,
        format: winston.format.combine(
            winston.format.simple()
        )
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
        format: winston.format.combine(
            winston.format.simple()
        )
    },
};

var logger = new winston.createLogger({
    transports: [
        new (winston.transports.File)(options.file),
        new (winston.transports.Console)(options.console)
    ],
    exitOnError: false, // do not exit on handled exceptions
});

logger.stream = {
    write: (message, encoding) => {
      logger.info(message);
    }
};

module.exports = logger;