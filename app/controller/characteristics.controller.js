const db = require('../config/db.config');
const Characteristics = db.characteristics;

exports.create = async (req, res) => {
    console.log('PRUEBAAAA', req.body.characteristics);
    const createCharacteristics = await Characteristics.create({
        age: req.body.characteristics.age,
        sex: req.body.characteristics.sex,
        weight: req.body.characteristics.weight,
        height: req.body.characteristics.height
    });
    res.status(200).json({
        message: "Encuesta completada",
        data: createCharacteristics
    });
};