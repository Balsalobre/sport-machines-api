
const db = require('../config/db.config');
const Machine = db.machine;


exports.create = async (req, res) => {
  const img_ext = req.file.mimetype;
  const allow_format = ['image/jpeg', 'image/png', 'image/gif', 'image/jpg'];
  if(!allow_format.includes(img_ext)) {
    return res.status(415).send('Archivo de imagen no soportado');
  }
  const createMachine = await Machine.create({  
    name: req.body.name,
    description: req.body.description,
    serialNumber: req.body.serialNumber,
    latitude: req.body.latitude,
    longitude: req.body.longitude,
    dataSheet: null,
    imageType: req.file.mimetype,
    imageOriginalName: req.file.originalname,
    imageNewName: req.file.filename
  });
  res.status(200).json({
    message: "Se ha guardado una nueva máquina",
    data: createMachine
  });
};

// Listar todas las Máquinas
exports.findAll = async (req, res) => {
  const machines = await Machine.findAll();
    res.status(200).send(machines);
};

// Encontrar una máquina por id
exports.findByPk = async (req, res) => {	
  const machine = await Machine.findByPk(req.params.machineId);
  res.send(machine);
};

// Actualizar una máquina
exports.update = async (req, res) => {
	const id = req.params.machineId;
	const machine = await Machine.update( {
      name: req.body.name,
      description: req.body.description,
      serialNumber: req.body.serialNumber,
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      imageType: req.file.mimetype,
      imageName: req.file.originalname },
		{ where: {id: id} }
  );
  res.status(200).send("Actualizada máquina con id: " + id);
};

// Borrar una máquina
exports.delete = async (req, res) => {
	const id = req.params.machineId;
	const macbine = await Machine.destroy({
	  where: { id: id }
	});
    res.status(200).send({ msg: `Borrada máquina con id + ${id}`});
};

// descargar un fichero
exports.download = async (req, res) => {
  const uploadFolder = __basedir + '/resources/machines/uploads/';
  const filename = req.params.machineImg;
	await res.download(uploadFolder + filename);
}

// Subir un pdf
exports.uploadpdf = async (req, res) => {
  const ext = req.file.mimetype;
  console.log(ext);
  if(ext !== 'application/pdf') {
    return res.status(409).send('Archivo no soportado, sólo PDF');
  }
  const id = req.params.machineId;
	const machine = await Machine.update( {
    dataSheet: req.file.originalname },
    {where: {id: id}
  });
  res.status(200).send(`Subido pdf para máquina con id: ${id}`);
}