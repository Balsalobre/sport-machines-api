const jwt = require('jsonwebtoken');
const passport = require('passport');
const GooglePlusTokenStrategy = require('passport-google-plus-token');
const FacebookTokenStrategy = require('passport-facebook-token');
const TwitterTokenStrategy = require('passport-twitter-token');
const db = require('../config/db.config');
const keys = require('../config/keys');
const UserOauth = db.userOauth;

/**
 * 
 * Función para comprobar si existe un usuario en la base de datos
 * y de no ser así crea uno nuevo
 * 
 */

async function ifNotExistCreate (profile, done) {
    // Check whether this current user exist in our DB
    const existingUser = await UserOauth.findOne({
        where: { id_provider: profile.id }
    });
    if (existingUser) {
        console.log('El usuario ya existe en la base de datos');
        return done(null, existingUser);
    }
    console.log('El usuario no existe en la base de datos, creamos uno nuevo');
    // If new account
    const newUser = await UserOauth.create({
        id_provider: profile.id,
        provider: profile.provider,
        name: profile.displayName,
        email: profile.emails[0].value,
        role: 'USER'
    });
    done(null, newUser);
}

// GOOGLE OAUTH STRATEGY
passport.use('googleToken', new GooglePlusTokenStrategy({
    clientID: keys.google.clientID,
    cientSecret: keys.google.clientSecret 
}, async(accessToken, refreshToken, profile, done, res) => {
    // console.log('accessToken', accessToken);
    // console.log('refreshToken', refreshToken);
    // console.log('profile', profile);

    ifNotExistCreate(profile, done);
}));

// FACEBOOK OAUTH STRATEGY
passport.use('facebookToken', new FacebookTokenStrategy({
    clientID: keys.facebook.clientID,
    clientSecret: keys.facebook.clientSecret
}, async (accessToken, refreshToken, profile, done) => {
    // console.log('profile', profile);
    // console.log('accessToken', accessToken);
    // console.log('refreshToken', refreshToken);

    ifNotExistCreate(profile, done);

}));

// TWITTER OAUTH STRATEGY
passport.use('twitterToken', new TwitterTokenStrategy({
    consumerKey: keys.twitter.consumerKey,
    consumerSecret: keys.twitter.consumerSecret
}, async (Token, tokenSecret, profile, done) =>{
    // console.log('profile', profile);
    // console.log('Token', Token);
    // console.log('tokenSecret', tokenSecret);

    ifNotExistCreate(profile, done);
}));

exports.signup = (req, res) => {
    const dataUser = req.user.dataValues;
    // console.log('******', dataUser);
    const token = jwt.sign({ id: dataUser.id_provider }, keys.secretJWT, {
        expiresIn: 86400 // expires in 24 hours
    });
    res.status(200).send({
        auth: true,
        accessToken: token,
        username: dataUser.name,
        authorities: ["ROLE_USER"]
    });
}

exports.userContent = (req, res) => {
    console.log(req.userId);
	UserOauth.findOne({
		where: { id_provider: req.userId },
		attributes: ['name', 'email', 'role']
	}).then(UserOauth => {
		res.status(200).send({
			'description': '>>> User Contents!',
			'user': UserOauth
		});
	}).catch(err => {
		res.status(500).send({
			'description': 'Can not access User Page',
			'error': err
		});
	})
}