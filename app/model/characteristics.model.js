module.exports = (sequelize, Sequelize) => {
	const Characteristics = sequelize.define('characteristics', {
		age: {
		    type: Sequelize.INTEGER
	    },
	    sex: {
		    type: Sequelize.STRING
		},
	    weight: {
			type: Sequelize.DECIMAL
		},
		height: {
			type: Sequelize.DECIMAL
		}
	});
	return Characteristics;
}