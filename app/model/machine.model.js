module.exports = (sequelize, Sequelize) => {
	const Machine = sequelize.define('machines', {
		name: {
		  type: Sequelize.STRING
	  },
	  description: {
		  type: Sequelize.STRING
		},
	  serialNumber: {
			type: Sequelize.INTEGER
		},
		latitude: {
			type: Sequelize.DECIMAL(10,8)
		},
		longitude: {
			type: Sequelize.DECIMAL(11,8)
		},
    dataSheet: {
        type: Sequelize.STRING
		},
		imageType: {
		  type: Sequelize.STRING
	  },
		imageOriginalName: {
		  type: Sequelize.STRING
		},
		imageNewName: {
			type: Sequelize.STRING
		}
	});
	
	return Machine;
}