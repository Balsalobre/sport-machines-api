module.exports = (sequelize, Sequelize) => {
	const UserOauth = sequelize.define('users-oauth', {
	  id_provider: {
		  type: Sequelize.STRING
	  },
	  provider: {
		  type: Sequelize.STRING
    },
    name: {
        type: Sequelize.STRING
    },
	  email: {
		  type: Sequelize.STRING
		},
		role: {
			type: Sequelize.STRING
		}
	});
	return UserOauth;
}