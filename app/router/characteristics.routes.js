module.exports = function(app) {
    const characteristics = require('../controller/characteristics.controller');

    app.post('/api/characteristics', characteristics.create);
}