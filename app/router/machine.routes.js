module.exports = function(app) {
    
    const upload = require('../config/upload.config');
    const machines = require('../controller/machine.controller');

    app.post('/api/machines', upload.single("uploadimg"), machines.create);
    app.get('/api/machines', machines.findAll);
    app.get('/api/machines/:machineId', machines.findByPk);
    app.put('/api/machines/:machineId', upload.single("uploadimg"), machines.update);
    app.delete('/api/machines/:machineId', machines.delete);
    app.get('/api/get-img-machines/:machineImg', machines.download);
    app.put('/api/machines-pdf/:machineId', upload.single("uploadpdf"), machines.uploadpdf);
    app.get('/api/get-pdf-machines/:machineImg', machines.download);
}   