const verifySignUp = require('./verifySignUp');
const authJwt = require('./verifyJwtToken');
const passport = require('passport');
const passportGoogle = passport.authenticate('googleToken', { session: false });
const passportFacebook = passport.authenticate('facebookToken', { session: false });
const passportTwitter = passport.authenticate('twitterToken', { session: false });
require('../controller/user-oauth.controller');

const oauthSignup = require('../controller/user-oauth.controller');


module.exports = function (app) {

	const userController = require('../controller/user.controller');

	app.post('/api/auth/signup', [verifySignUp.checkDuplicateUserNameOrEmail, verifySignUp.checkRolesExisted], userController.signup);
	app.post('/api/auth/signin', userController.signin);

	// OAuth Routes
	app.post('/api/oauth/google', passportGoogle, oauthSignup.signup);
	// Para conseguir el token ir a: https://developers.facebook.com/tools/accesstoken/
	app.post('/api/oauth/facebook', passportFacebook, oauthSignup.signup);
	app.post('/api/oauth/twitter', passportTwitter, oauthSignup.signup);

	app.get('/api/test/user-oauth', [authJwt.verifyToken], oauthSignup.userContent);
	app.get('/api/test/user', [authJwt.verifyToken], userController.userContent);
	app.get('/api/test/pm', [authJwt.verifyToken, authJwt.isPmOrAdmin], userController.managementBoard);
	app.get('/api/test/admin', [authJwt.verifyToken, authJwt.isAdmin], userController.adminBoard);
}