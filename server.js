require('express-async-errors');	// Handling and Logging Errors
const express = require('express');
const morgan  = require('morgan');
const winston = require('./app/config/wiston.config'); 
const logger = require('./app/config/wiston.config');
const app = express();
const bodyParser = require('body-parser');

global.__basedir = __dirname;

app.use(bodyParser.json());
app.use(morgan('combined', { stream: winston.stream }));

//Configuración cabeceras http
// app.use(function (req, res, next) {
// 	res.header("Access-Control-Allow-Origin", "*");
// 	res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
// 	next();
// });

const cors = require('cors')
const corsOptions = {
  origin: 'http://localhost:4200'
}
 
app.use(cors(corsOptions))

// Carga de rutas de la aplicación
require('./app/router/user.routes')(app);
require('./app/router/machine.routes')(app);
require('./app/router/characteristics.routes')(app);

const db = require('./app/config/db.config.js');

const Role = db.role;

// force: Si lo ponemos a true borraremos las tablas si ya existen
// force: Si lo ponemos a false no borraremos las tablas existentes
db.sequelize.sync({force: false}).then(() => {
  //console.log('Drop and Resync with { force: true }');
  initial();
});

// Create a Server
const server = app.listen(8080, function () {

	const host = server.address().address
	const port = server.address().port

	logger.info("App listening at http://%s:%s", host, port)
})

function initial() {
	Role.create({
		id: 1,
		name: "USER"
	});

	Role.create({
		id: 2,
		name: "PM"
	});

	Role.create({
		id: 3,
		name: "ADMIN"
	});
}

