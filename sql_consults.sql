SELECT users.name, users.email, roles.name AS role
	FROM users
LEFT OUTER JOIN user_roles
	ON users.id = user_roles.userId
LEFT OUTER JOIN roles
	ON user_roles.roleId = roles.id
UNION
SELECT name, email, role FROM `users-oauths`;

SELECT `users`.`id`, `users`.`name`, `users`.`username`, `users`.`email`,
`roles`.`id` AS `roles.id`,
`roles`.`name` AS `roles.name`,
`roles->user_roles`.`createdAt` AS `roles.user_roles.createdAt`,
`roles->user_roles`.`updatedAt` AS `roles.user_roles.updatedAt`,
`roles->user_roles`.`roleId` AS `roles.user_roles.roleId`,
`roles->user_roles`.`userId` AS `roles.user_roles.userId`
FROM `users` AS `users`
LEFT OUTER JOIN ( `user_roles` AS `roles->user_roles`
INNER JOIN `roles` AS `roles` ON `roles`.`id` = `roles->user_roles`.`roleId`)
ON `users`.`id` = `roles->user_roles`.`userId` WHERE `users`.`id` = 1;

SELECT `name`, `email`, `role` FROM `users-oauths` AS `users-oauth` WHERE `users-oauth`.`id_provider` = '1088170980369936385' LIMIT 1;